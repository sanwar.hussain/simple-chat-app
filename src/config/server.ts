type DefaultConfig = {
	serverURL: string
}

const dev: DefaultConfig = {
	serverURL: 'http://localhost:5000',
}
const prod: DefaultConfig = {
	serverURL: 'https://simple-chat-server0.herokuapp.com/',
}

let config: DefaultConfig

if (process.env.NODE_ENV === 'production') config = prod
else config = dev

export default config
//test
