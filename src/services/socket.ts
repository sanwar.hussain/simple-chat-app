import { io } from 'socket.io-client'
import config from '../config/server'

const serverUrl: string = config.serverURL
const socket = io(serverUrl, { autoConnect: false })

if (!serverUrl) throw new Error('Fatal: Invalid server string!')

export default socket
