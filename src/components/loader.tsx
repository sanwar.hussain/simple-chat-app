import { FC } from 'react'
import Lottie from 'react-lottie'
import error from '../assets/animations/error.json'
import loaderJson from '../assets/animations/loader.json'

import '../styles/loader.css'

type Props = {
	isServerDown: boolean
}

const Loader: FC<Props> = ({ isServerDown }) => {
	const lottieOptions = { autoplay: true }
	return (
		<div className='loader-master-container'>
			{isServerDown ? (
				<>
					<Lottie
						options={{ ...lottieOptions, animationData: error, loop: false }}
						height={100}
						width={100}
					/>
					<p className='info-text'>
						The server seems down, Please try again later.
					</p>
				</>
			) : (
				<>
					<Lottie
						options={{
							...lottieOptions,
							loop: true,
							animationData: loaderJson,
						}}
						height={150}
						width={150}
					/>
					<p className='info-text'>Preparing your chat room..</p>
				</>
			)}
		</div>
	)
}

export default Loader
