import moment from 'moment'
import { FC } from 'react'
import socket from '../services/socket'

import '../styles/message.css'

type Msg = {
	type?: string
	username?: string
	userId: string
	timestamp: Date
	message: string
}
type Props = {
	data: Msg
}

const Message: FC<Props> = ({ data }) => {
	return (
		<>
			{!data.type ? (
				<div
					className={
						data.userId === socket.id
							? `message-master-container message-right`
							: 'message-master-container'
					}
				>
					<div className='message-container '>
						<p className='user-name'>
							{data.userId === socket.id ? 'You' : data.username}
						</p>
						<div className='message-content'>
							<p className='message'>{data.message}</p>
							<p
								className={
									data.userId === socket.id
										? 'message-time time-right'
										: 'message-time'
								}
							>
								{moment(data.timestamp).fromNow()}
							</p>
						</div>
					</div>
				</div>
			) : (
				<div className='notification-container'>
					<div className='notification-message'>
						<img src={require('../assets/icons/info.png')} alt='' />
						<p className='notification-text'>
							{`${data?.username} ${
								data.type === 'connect' ? 'joined' : 'left'
							}`}
						</p>
					</div>
				</div>
			)}
		</>
	)
}

export default Message
