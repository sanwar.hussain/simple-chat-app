import { FC, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Loader from '../components/loader'
import config from '../config/server'
import socket from '../services/socket'

import '../styles/welcome.css'

type Props = {}

const WelcomeScreen: FC<Props> = (props) => {
	const navigate = useNavigate()

	const [isReady, setIsReady] = useState(false)
	const [isServerDown, setIsServerDown] = useState(false)
	const [username, setUsername] = useState('')
	const [roomKey, setRoomKey] = useState('')
	const [showError, setShowError] = useState(false)

	function handleStartChat() {
		if (!username) return setShowError(true)
		socket.emit('notify-user', { type: 'connect', username }, roomKey)
		navigate('/chat', {
			state: { username, roomKey: roomKey ? roomKey : socket.id },
		})
	}

	useEffect(() => {
		fetch(config.serverURL)
			.then((data) => {
				if (data.status === 200) {
					socket.connect()
					setIsReady(true)
				}
			})
			.catch((err) => {
				setIsServerDown(true)
			})
	}, [])

	return (
		<div className='welcome-master-container'>
			{isReady ? (
				<div className='welcome-container'>
					<p className='app-name'>Simple Chat</p>
					<p className='app-tag'>
						Set a name, start chatting. No logs. Simple!
					</p>
					<div className='user-name-input-container'>
						<label htmlFor='topic' className='label-text'>
							Username
						</label>
						<input
							type='text'
							className='text-input input-user-name'
							placeholder='Pick a classy one!'
							onFocus={() => setShowError(false)}
							value={username}
							onChange={(e) => setUsername(e.target.value)}
						/>
						<label htmlFor='topic' className='label-text'>
							Room Key (if have)
						</label>
						<input
							type='text'
							className='text-input input-user-name'
							placeholder='Create/Input a room key'
							onFocus={() => setShowError(false)}
							value={roomKey}
							onChange={(e) => setRoomKey(e.target.value)}
						/>
					</div>
					<button
						className='btn-start-chat'
						name='Start Chatting'
						onClick={handleStartChat}
					>
						Start Chatting
					</button>
					<p className={`error-message ${!showError && 'hide-error'}`}>
						Pick a username first
					</p>
				</div>
			) : (
				<Loader isServerDown={isServerDown} />
			)}
		</div>
	)
}

export default WelcomeScreen
