import { FC, useState } from 'react'
import socket from '../services/socket'
// @ts-ignore
import InputEmoji from 'react-input-emoji'

import { useLocation, useNavigate } from 'react-router-dom'
import Message from '../components/message'
import '../styles/chat.css'

type Props = {}

const ChatScreen: FC<Props> = (props) => {
	const navigate = useNavigate()
	const location = useLocation()

	const { username, roomKey } = location.state

	const [allMessages, setAllMessages] = useState<Array<any>>([])
	const [messageInput, setMessageInput] = useState('')

	socket.on('receive-message', (newMessage) => {
		setAllMessages([...allMessages, newMessage])
	})

	function handleMessageInput(message: string) {
		const userId: string = socket.id
		const newMessage = {
			username,
			userId,
			timestamp: new Date(),
			message,
		}
		setAllMessages([...allMessages, newMessage])
		socket.emit('send-message', newMessage, roomKey)
	}

	function handleEndChat() {
		socket.emit('notify-user', { type: 'disconnect', username }, roomKey)
		navigate('/')
	}

	return (
		<div className='chat-master-container'>
			<div className='chat-container'>
				<div className='chat-header'>
					<img
						src={require('../assets/images/lgo.png')}
						alt=''
						className='app-logo'
					/>
					<p className='room-key'>
						Room Key :{' '}
						<span className='key'>{roomKey ? roomKey : socket.id}</span>
					</p>
					<button className='btn-end-chat' onClick={handleEndChat}>
						End Chat
					</button>
				</div>
				<div className='chat-body'>
					{allMessages.map((message, index) => (
						<Message key={index} data={message} />
					))}
				</div>
				<div className='chat-action'>
					<InputEmoji
						value={messageInput}
						onChange={(value: string) => setMessageInput(value)}
						cleanOnEnter
						onEnter={() => handleMessageInput(messageInput)}
						placeholder='Type a message'
					/>
				</div>
			</div>
		</div>
	)
}

export default ChatScreen
