import { FC } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import ChatScreen from './screens/chat'
import WelcomeScreen from './screens/welcome'

type Props = {}

const App: FC<Props> = (props) => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<WelcomeScreen />} />
				<Route path='/chat' element={<ChatScreen />} />
				<Route path='*' element={<p>invalid route</p>} />
			</Routes>
		</BrowserRouter>
	)
}

export default App
